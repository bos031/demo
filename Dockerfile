FROM openjdk:8-jre-alpine

ENV VERTICLE_FILE vertx-fileupload-1.0-SNAPSHOT-fat.jar
ENV VERTICLE_HOME /usr/verticles

RUN mkdir -p $VERTICLE_HOME && \
    addgroup -g 1000 -S testuser && \
    adduser -u 1000 -S testuser -G testuser


# Copy App and resources
COPY target/$VERTICLE_FILE /dip/service/
COPY index.html $VERTICLE_HOME/html/

RUN chown -R testuser:root $VERTICLE_HOME && \
    chown -R testuser:root /dip/service && \
    chmod -R ug+rwx $VERTICLE_HOME && \
    chmod -R ug+rwx /dip/service


EXPOSE 8080

USER testuser
# Launch the verticle
WORKDIR $VERTICLE_HOME
ENTRYPOINT ["sh","-c"]
CMD ["exec java -jar -Dvertx.cacheDirBase=/tmp /dip/service/$VERTICLE_FILE"]