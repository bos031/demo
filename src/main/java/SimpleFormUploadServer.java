import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.logging.SLF4JLogDelegateFactory;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.LoggerHandler;
import io.vertx.ext.web.handler.StaticHandler;
import org.slf4j.Logger;

import java.util.Optional;

public class SimpleFormUploadServer extends AbstractVerticle {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(SimpleFormUploadServer.class);


    @Override
    public void start() {
        System.setProperty(LoggerFactory.LOGGER_DELEGATE_FACTORY_CLASS_NAME, SLF4JLogDelegateFactory.class.getName());

        vertx.fileSystem().mkdirs("uploads", handler -> {
        });
        HttpServer httpServer = vertx.createHttpServer();
        Router router = Router.router(vertx);
        router.route().handler(LoggerHandler.create(LoggerHandler.DEFAULT_FORMAT));
        router.route("/uploads/*").handler(StaticHandler.create("uploads").setDirectoryListing(true).setCachingEnabled(false));
        router.route("/").handler(StaticHandler.create("html").setCachingEnabled(false));
        router.post("/upload").handler(BodyHandler.create().setUploadsDirectory("uploads"));

        router.post("/upload").handler(context -> {
            for (FileUpload f : context.fileUploads()) {

                log.info("Uploaded: name -> {}, size -> {}",f.fileName(),f.size());
                log.debug("Uploaded: name -> {}, size -> {}",f.fileName(),f.size());


                context.response().setStatusCode(201);
                context.response().putHeader("Content-Type", "application/json");
                final String response = new JsonObject()
                        .put("success", true)
                        .put("timestamp", System.nanoTime())
                        .put("status", 201)
                        .put("path", context.request().path()).encode();

                context.response().end(response);
            }
        });

        httpServer.requestHandler(router::accept).listen(8080, handler -> {
            if(handler.succeeded()){
                log.info("Service started successfully.");
            }else{
                log.error(handler.cause().getLocalizedMessage());
            }
        });
    }


}